---
title: "Sobre"
description: "Alguns anos atrás, enquanto visitava ou, em vez disso, remexia em Notre-Dame, o autor deste livro encontrou, num recanto obscuro de uma das torres, a seguinte palavra, gravada à mão na parede: - ANANKE."
featured_image: ''
slug: "sobre"
translationKey: "about"
---
{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}}

O Corcunda de Notre-Dame (em francês: Notre-Dame de Paris) é um romance romântico / gótico francês de Victor Hugo, publicado em 1831. O título original em francês refere-se à Catedral de Notre Dame, na qual a história está centrada. O tradutor inglês Frederic Shoberl nomeou o romance O Corcunda de Notre Dame em 1833 porque, na época, os romances góticos eram mais populares que os romances da Inglaterra. A história se passa em Paris, França, no final da Idade Média, durante o reinado de Luís XI.
