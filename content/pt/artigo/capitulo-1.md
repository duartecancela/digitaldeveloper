---
date: 2017-04-09T10:58:08-04:00
description: "O Grande Salão"
featured_image: "/images/Pope-Edouard-de-Beaumont-1844.jpg"
tags: ["scene"]
title: "Capítulo I: O Grande Salão"
translationKey: "chapter-1"
---

Trezentos e quarenta e oito anos, seis meses e dezenove dias atrás
hoje, os parisienses acordavam ao som de todos os sinos no triplo
circuito da cidade, a universidade e a cidade tocando um barulho completo.

O dia 6 de janeiro de 1482 não é, no entanto, um dia em que a história
preservou a memória. Não havia nada de notável no evento que assim
coloque os sinos e os burgueses de Paris em um fermento desde o início da manhã.
Não foi um ataque dos Picard nem dos Borgonheses, nem uma caçada
liderados em procissão, nem uma revolta de estudiosos na cidade de Laas, nem
uma entrada de "nosso senhor muito pavor, monsieur o rei", nem mesmo uma bonita
enforcamento de ladrões masculinos e femininos pelos tribunais de Paris. Nem foi
a chegada, tão frequente no século XV, de alguns emplumados e
Embaixada embaraçada. Fazia apenas dois dias desde a última cavalgada daquele
natureza, a dos embaixadores flamengos encarregados de concluir a
casamento entre o delfim e Margarida da Flandres, havia feito sua
entrada em Paris, para grande aborrecimento do sr. cardeal de Bourbon,
que, para agradar ao rei, foram obrigados a assumir uma
semelhança amável com toda essa multidão rústica de burgomestres flamengos, e
regalá-los em seu Hôtel de Bourbon, com uma moralidade muito "bonita",
sátira alegórica e farsa ", enquanto uma chuva forte encharcava a
tapeçarias magníficas à sua porta.

O que colocou toda a população de Paris em tumulto, como Jehan de Troyes
expressa em 6 de janeiro a dupla solenidade, unida
desde tempos imemoriais, da Epifania e da Festa dos Loucos.

Naquele dia, haveria uma fogueira na Place de Grève, um poste na
a Chapelle de Braque e um mistério no Palais de Justice. Teve
ao som da trombeta, na noite anterior em todos os lugares.
estradas transversais, pelos homens do reitor, vestidos de bonito, baixo, sem mangas
casacos de camelot violeta, com grandes cruzes brancas sobre os seios.

Então a multidão de cidadãos, homens e mulheres, tendo fechado suas casas e
lojas, lotadas de todas as direções, no início da manhã, em direção a algum
os três pontos designados.

Cada um fez sua escolha; um, a fogueira; outro, o mastro; outro,
a peça de mistério. Deve-se afirmar, em homenagem ao bom senso do
espreguiçadeiras de Paris, que a maior parte dessa multidão dirigia seus
passos em direção à fogueira, que estava bastante na estação, ou em direção ao
peça de mistério, que seria apresentada no grande salão do Palais de
Justiça (os tribunais), que era bem coberta e murada; e essa
os curiosos deixaram o mastro pobre e escassamente florido a tremer sozinho
sob o céu de janeiro, no cemitério da capela de Braque.

A população aglomerou-se nas avenidas dos tribunais em particular, porque
eles sabiam que os embaixadores flamengos, que chegaram dois dias
anteriormente, pretendia estar presente na representação do mistério,
e na eleição do Papa dos tolos, que também aconteceria
no grande salão.

Não foi fácil naquele dia forçar alguém a entrar naquele grande
embora tenha sido considerado o maior recinto coberto do mundo
mundo (é verdade que Sauval ainda não havia medido o grande salão de
Castelo de Montargis). A praça do palácio, sobrecarregada de pessoas,
ofereceu aos curiosos observadores nas janelas o aspecto de um mar; para dentro
quais cinco ou seis ruas, como tantas bocas de rios, descarregavam a cada
momento inundações frescas de cabeças. As ondas dessa multidão, aumentadas
incessantemente, arremessado contra os ângulos das casas que projetavam aqui
e lá, como tantos promontórios, na bacia irregular do
Lugar, colocar. No centro da imponente fachada gótica * do palácio, o grande
escada, subia e descia incessantemente por uma corrente dupla que,
depois de partir no local de pouso intermediário, fluiu em ondas largas
ao longo de suas encostas laterais - a grande escadaria, eu digo, escorria
incessantemente no lugar, como uma cascata em um lago. Os gritos, os
o riso, o pisoteio daqueles milhares de pés, produziu um grande ruído
e um grande clamor. De tempos em tempos, esse barulho e clamor redobravam;
a corrente que levou a multidão em direção à grande escadaria fluiu
para trás, ficou perturbado, formou redemoinhos. Isso foi produzido pelo
buffet de um arqueiro, ou o cavalo de um dos sargentos do reitor, que
chutado para restaurar a ordem; uma tradição admirável que a tutela tem
legado ao constablery, o constablery ao _maréchaussée_,
o _maréchaussée_ ao nosso_gendarmeri_ de Paris.
